import {ActionType} from './ActionTypes';

export function pressButton() {
    return (dispatch, getState) => {

        // Do all kinds of stuff and on success:
        dispatch({type: ActionType.I_PRESSED_A_BUTTON});
    }
}