import React, {Component} from 'react';
import {connect} from 'react-redux';
import {View} from 'react-native';

import MainStackNavigator from './Navigation';

class App extends Component {
    render() {
        let viewToDisplay = (<MainStackNavigator/>);

        return (
            <View style={{flex: 1}}>
                {viewToDisplay}
            </View>
        );
    }
}

function mapStateToProps(state) {
    return {};
}

function mapDispatchToProps(dispatch) {
    return {};
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App);
