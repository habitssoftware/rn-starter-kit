import React, {Component} from 'react';
import {StackNavigator, TabNavigator} from 'react-navigation';

import View1 from '../views/View1';
import View2 from '../views/View2';
import View3 from '../views/View3';



const MainTabNavigator = TabNavigator({
    View1: {screen: View1},
    View2: {screen: View2},
});

const MainStackNavigator = StackNavigator({
    MainScreenNavigator: {screen: MainTabNavigator},
    View3: {screen: View3}
});

export default MainStackNavigator;