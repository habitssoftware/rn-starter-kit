# Introduction #
This package is an installer for react native, including some packages I like to use. It is mainly a personal project, so the structure of the folders and files may differ from what you are used to. I like doing it this way :). 

# Dependencies #
- React
- React Native
- React Navigation
- Redux
- Axios
- Font Awesome


# Installation #
- Install the react-native-cli, using using `sudo yarn add react-native-cli -g`.
- Install this package using `sudo yarn add rn-starter-kit -g`.
- Run `rn-starter-kit init <ProjectName>` in your projects folder. A new folder with the project will be created.

# Usage #
This node package is only an installer that builds a react native project with some essentials. For that reason, the only argument available is 'init'. All other commands have to be done using the react native cli.


# iOS thingy #

I have added Font Awesome by default. Before you can run the app on iOS, you need to link the font in your xCode project. Follow these steps:
- Drag the FontAwesome font from `src/static/fonts/` to the root of your app in xCode (the same level as Libraries, Products, YourApp and YourAppTests).
- A popup will show and make sure that at least the checkbox for YourApp is checked in the section 'Add to targets'. You do not need to copy the files and only a reference is enough.
- I have already included the font in your Info.plist (`Yourapp/Info.plist`), but if you want to add other custom fonts, make sure you add yours here as well. 
- If you already had some stuff running, restart all of it; both packager and the build of the ios project.
- There you go!