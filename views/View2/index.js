import React, {Component} from 'react';
import {View, Text, Button} from 'react-native';
import {connect} from 'react-redux';

import FontAwesome from '../../elements/FontAwesome';
import globalStyles from '../../shared/globalStyles';
import styles from './styles';

import {pressButton} from '../../actions/MyActions';

class View2 extends Component {
    static navigationOptions = {
        title: 'View 2',
        tabBarLabel: 'View 2',
        tabBarIcon: ({tintColor})=>(<FontAwesome icon="minus" style={{...globalStyles.tabBarIcon, color: tintColor}}/>)
    };

    constructor(props){
        super(props);

        this.state = {
            buttonPressed: this.props.buttonPressed
        }
    }

    componentWillReceiveProps(nextProps){
        this.setState({
            buttonPressed: nextProps.buttonPressed
        });
    }

    render(){
        return(
            <View style={globalStyles.mainContainer}>
                <Text style={styles.header}>This is View 2</Text>
                <Button title="Press this button for a redux action" onPress={this.props.pressButton} />
                <Text style={{alignSelf: 'center'}}>Button has been pressed: {this.state.buttonPressed.toString()}</Text>
            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        buttonPressed: state.myReducer.buttonPressed
    };
}

function mapDispatchToProps(dispatch) {
    return {
        pressButton: () => dispatch(pressButton()),
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(View2);