import React, {Component} from 'react';
import {connect} from 'react-redux';
import {View, Text, Button} from 'react-native';

import globalStyles from '../../shared/globalStyles';
import styles from './styles';

import ExampleElement from '../../elements/ExampleElement';

class View3 extends Component {
    static navigationOptions = {
        title: 'Awesome view'
    };

    render(){
        return(
            <View style={globalStyles.mainContainer}>
                <Text style={styles.header}>This is View 3</Text>
                <ExampleElement viewName="the most awesome view" />
            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
    };
}

function mapDispatchToProps(dispatch) {
    return {
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(View3);