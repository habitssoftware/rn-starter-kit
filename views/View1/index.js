import React, {Component} from 'react';
import {connect} from 'react-redux';
import {View, Text, Button} from 'react-native';

import FontAwesome from '../../elements/FontAwesome';
import globalStyles from '../../shared/globalStyles';
import styles from './styles';

class View1 extends Component {
    static navigationOptions = {
        title: 'View 1',
        tabBarLabel: 'View 1',
        tabBarIcon: ({tintColor})=>(<FontAwesome icon="plus" style={{...globalStyles.tabBarIcon, color: tintColor}}/>)
    };

    render(){
        return(
            <View style={globalStyles.mainContainer}>
                <Text style={styles.header}>This is View 1</Text>

                <Button
                    title="Press this button to go to View 3"
                    onPress={()=>{
                        this.props.navigation.navigate('View3');
                    }}
                />
            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
    };
}

function mapDispatchToProps(dispatch) {
    return {
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(View1);