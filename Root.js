import React, { Component, PropTypes } from 'react';

import { Provider } from 'react-redux';
import App from './components/App';


export default class Root extends Component {

    render() {
        return (
            <Provider store={this.props.store}>
                <App />
            </Provider>
        );
    }

}

Root.propTypes = {
    store: PropTypes.object.isRequired
}

String.prototype.ucfirst = function() {
    return this.charAt(0).toUpperCase() + this.toLowerCase().slice(1);
};
String.prototype.isNumber = function() {
    return this.charAt(0).match(/^[0-9]+$/);
};
String.prototype.isLetter = function() {
    return this.charAt(0).match(/^[a-zA-Z]+$/);
};