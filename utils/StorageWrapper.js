import {AsyncStorage, Platform} from 'react-native';

let mobileStorageAccessible = Platform.OS === 'ios' || Platform.OS === 'android';
let sessionStorageAccessible = true;
let fakeStorage = {};

const setItemToStorage = (key, value) => {
    if(mobileStorageAccessible) {
        try {
            AsyncStorage.setItem(key, value);
        } catch(error) {
            mobileStorageAccessible = false;
            fakeStorage[key] = value;
        }
    } else {
        fakeStorage[key] = value;
    }
};
const getItemFromStorage = (key) => {
    let result;
    try {
        result = mobileStorageAccessible ? AsyncStorage.getItem(key) : fakeStorage[key];
    } catch(error) {
        result = fakeStorage[key];
    }

    return result;
};
const deleteItemFromStorage = (key) => {
    try {
        sessionStorageAccessible ? AsyncStorage.removeItem(key) : fakeStorage[key] = null;
    } catch(error) {
        fakeStorage[key] = null;
    }
};

export const storeItem = (key, value) => setItemToStorage(key, value);
export const getItem = (key) => getItemFromStorage(key);
export const deleteItem = (key) => deleteItemFromStorage(key);