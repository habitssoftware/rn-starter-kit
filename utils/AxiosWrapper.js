import axios from 'axios';

let baseUrl = 'http://yoururl.com';
let instance = axios.create({
    baseURL: baseUrl,
    timeout: 10000,
    headers: {
        'Accept': 'application/json'
    }
});

const fetch = (type, data) => {
    instance[type](data.route, data.data)
        .then(function(response) {
            data.success(response.data);
        })
        .catch(function(error) {
            data.fail(error);
        });
};


export const post = data => fetch('post', data);
export const get = data => fetch('get', data);
export const put = data => fetch('put', data);