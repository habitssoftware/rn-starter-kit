import immutable from 'seamless-immutable';

import { ActionType }  from '../actions/ActionTypes';

const INITIAL_STATE = immutable({
    buttonPressed: false
});


export default function (state = INITIAL_STATE, action) {
    switch (action.type) {

        case ActionType.I_PRESSED_A_BUTTON:
            return state.merge({ buttonPressed: true });

        default:
            return state
    }
}