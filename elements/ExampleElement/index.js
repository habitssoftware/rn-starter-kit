import React, {Component} from 'react';
import {View, Text} from 'react-native';

import styles from './styles';

export default class ExampleElement extends Component{
    render(){
        return(
            <View>
                <Text style={styles.content}>This is an element that has been imported from {this.props.viewName}</Text>
            </View>
        )
    };
}