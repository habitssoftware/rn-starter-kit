import React, {Component} from 'react';
import {Text, StyleSheet} from 'react-native';

import {Icons} from 'react-native-fontawesome';

const styles = StyleSheet.create({
    icon: {
        fontFamily: 'FontAwesome',
        backgroundColor: 'transparent',
        fontWeight: 'normal'
    },
});

export default class Icon extends Component {
    render() {
        let iconStyles = [styles.icon];

        if(this.props.style){
            iconStyles.push(this.props.style);
        }

        return (
            <Text style={iconStyles}>
                {Icons[this.props.icon]}
            </Text>
        );
    }
}
